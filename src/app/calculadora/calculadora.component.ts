import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.css']
})
export class CalculadoraComponent implements OnInit {

  operando: string = '';
  operandoAlt: string = '';
  _operador: string = '';
  constructor() { }

  ngOnInit(): void {
  }

  borrar() {
    this.operando = '';
    this.operandoAlt = '';
  }

  invertir() {
    this.operando = `-${this.operando}`;
  }
  operador(op: string) {
    this._operador = op;
    if (this.operando !== '') {
      this.operandoAlt = this.operando;
      this.operando = '';
    }
  }

  add(operando: string) {
    this.operando += operando;
  }
  calcularResultado() {
    this.operando = (this.calcular()).toString();
  }


  calcular(): number {
    if (this._operador === 'division') {
      return (Number.parseFloat(this.operandoAlt) / (Number.parseFloat(this.operando)))
    }
    if (this._operador === 'multiplicacion') {
      return (Number.parseFloat(this.operandoAlt) * (Number.parseFloat(this.operando)))
    }
    if (this._operador === 'resta') {
      return (Number.parseFloat(this.operandoAlt) - (Number.parseFloat(this.operando)))
    }
    if (this._operador === 'suma') {
      return (Number.parseFloat(this.operandoAlt) + (Number.parseFloat(this.operando)))
    }
    return -1;
  }

}
